#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	int arr[9], i;
	//memset(arr, 0, 9 * sizeof(int));
	for (i = 0; i < 9; i++)
		arr[i] = 0;
	for (i = 0; i < 9; i++)
	{
		do
		{
			cout << "Bitte geben Sie die " << (i + 1) << "-te Zahl ein: ? ";
			cin >> arr[i];
		} while ((arr[i] < 1) || (arr[i] > 6));
	}
	for (i = 0; i < 9; i++)
		cout << "Die " << (i + 1) << "-te eingegebene Zahl lautetete: " << arr[i] << endl;
	system("PAUSE");
	return 0;
}