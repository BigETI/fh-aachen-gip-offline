#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	int b, h;
	cout << "Bitte geben Sie die Breite des Parallelogramms ein: ";
	cin >> b;
	cout << "Bitte geben Sie die Hoehe des Parallelogramms ein: ";
	cin >> h;
	for (int i = 0, j; i < h; i++)
	{
		for (j = 0; j < i; j++)
			cout << '.';
		for (j = 0; j < b; j++)
		{
			if ((i == 0) || (i == (h - 1)) || (j == 0) || (j == (b - 1)))
				cout << '*';
			else
				cout << '+';
		}
		cout << endl;
	}
	cout << endl;
	system("PAUSE");
	return 0;
}