#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	static const int ARR_LEN = 6;
	int i, t, c = 0;
	bool arr[ARR_LEN];
	//memset(arr, 0, ARR_LEN * sizeof(bool));
	for (i = 0; i < ARR_LEN; i++)
		arr[i] = false;
	for (i = 0; i < 9; i++)
	{
		do
		{
			cout << "Bitte geben Sie die " << (i + 1) << "-te Zahl ein: ? ";
			cin >> t;
			if ((t >= 1) && (t <= 6))
			{
				if (!(arr[t - 1]))
				{
					arr[t - 1] = true;
					++c;
				}
			}
		} while ((t < 1) || (t > 6));
	}
	cout << "In der Eingabe kamen " << c << " unterschiedliche Zahlen vor.\n";
	system("PAUSE");
	return 0;
}