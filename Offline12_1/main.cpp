#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	int hoehe, stufenhoehe, breite, step = 0, i, j;
	string *canvas = nullptr;
	cout << "Bitte die Hoehe des Tannenbaums eingeben: ? ";
	cin >> hoehe;
	cout << "Bitte die Hoehe jeder Stufe eingeben: ? ";
	cin >> stufenhoehe;
	try
	{
		canvas = new string[hoehe];
		breite = hoehe * 2;
		for (i = 0; i < hoehe; i++)
		{
			for (j = 0; j < breite; j++)
				canvas[i] += '+';
			canvas[i][hoehe - step] = '*';
			canvas[i][hoehe + step] = '*';
			if (i > 0)
			{
				if ((i % (stufenhoehe - 1)) == 0)
				{
					canvas[i][(hoehe + 1) - step] = '*';
					canvas[i][(hoehe - 1) + step] = '*';
					step -= 2;
					canvas[i][hoehe - step] = '*';
					canvas[i][hoehe + step] = '*';
				}
			}
			++step;
		}

		for (i = 0; i < hoehe; i++)
			cout << canvas[i] << endl;
	}
	catch (...)
	{
		cerr << "Out of memory!" << endl;
	}
	delete[] canvas;
	system("PAUSE");
	return 0;
}