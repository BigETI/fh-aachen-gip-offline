#include <iostream>
#include <string>

using namespace std;

struct Person
{
	string nachname, vorname;
	int alter;
	char geschlecht;
};

enum FilterTypeEnum { FEMALE_MATURE = 1, MALE_MATURE, FEMALE_CHILD, MALE_CHILD };

void filter(Person personen[], int anzahl_person, FilterTypeEnum filter_type, int &count)
{
	int i;
	char geschlecht = '\0';
	bool result;
	switch (filter_type)
	{
	case FEMALE_MATURE: case FEMALE_CHILD:
		geschlecht = 'w';
		break;
	case MALE_MATURE: case MALE_CHILD:
		geschlecht = 'm';
		break;
	}
	for (i = 0; i < anzahl_person; i++)
	{
		if (personen[i].geschlecht == geschlecht)
		{
			result = false;
			switch (filter_type)
			{
			case FEMALE_MATURE: case MALE_MATURE:
				if (personen[i].alter >= 18)
					result = true;
				break;
			case FEMALE_CHILD: case MALE_CHILD:
				if (personen[i].alter < 18)
					result = true;
				break;
			}
			if (result)
			{
				cout << personen[i].nachname << ", " << personen[i].vorname << ", " << personen[i].geschlecht << ", " << personen[i].alter << endl;
				++count;
			}
		}
	}
}

int main(int argc, char *argv[])
{
	Person personen[100] = { { "Musterfrau1", "Petra1", 18, 'w' },
	{ "Mustermann1", "Klaus1", 18, 'm' },
	{ "Mustermaedchen1", "Lisa1", 1, 'w' },
	{ "Musterjunge1", "Jan1", 1, 'm' },
	{ "Musterfrau2", "Petra2", 19, 'w' },
	{ "Mustermann2", "Klaus2", 19, 'm' },
	{ "Mustermaedchen2", "Lisa2", 2, 'w' },
	{ "Musterjunge2", "Jan2", 2, 'm' },
	{ "Musterfrau3", "Petra3", 20, 'w' },
	{ "Mustermann3", "Klaus3", 20, 'm' },
	{ "Mustermaedchen3", "Lisa3", 3, 'w' },
	{ "Musterjunge3", "Jan3", 3, 'm' },
	{ "Musterfrau4", "Petra4", 21, 'w' },
	{ "Mustermann4", "Klaus4", 21, 'm' },
	{ "Mustermaedchen4", "Lisa4", 4, 'w' },
	{ "Musterjunge4", "Jan4", 4, 'm' },
	{ "Musterfrau5", "Petra5", 22, 'w' },
	{ "Mustermann5", "Klaus5", 22, 'm' },
	{ "Mustermaedchen5", "Lisa5", 5, 'w' },
	{ "Musterjunge5", "Jan5", 5, 'm' },
	{ "Musterfrau6", "Petra6", 23, 'w' },
	{ "Mustermann6", "Klaus6", 23, 'm' },
	{ "Mustermaedchen6", "Lisa6", 6, 'w' },
	{ "Musterjunge6", "Jan6", 6, 'm' },
	{ "Musterfrau7", "Petra7", 24, 'w' },
	{ "Mustermann7", "Klaus7", 24, 'm' },
	{ "Mustermaedchen7", "Lisa7", 7, 'w' },
	{ "Musterjunge7", "Jan7", 7, 'm' },
	{ "Musterfrau8", "Petra8", 25, 'w' },
	{ "Mustermann8", "Klaus8", 25, 'm' },
	{ "Mustermaedchen8", "Lisa8", 8, 'w' },
	{ "Musterjunge8", "Jan8", 8, 'm' }
	};
	int anzahl_personen = 32;
	int count = 0, i, filter_type;
	char c;
	bool escape = false;
	do
	{
		cout << "Eine weitere Person eingeben (j/n)? ";
		cin >> c;
		switch (c)
		{
		case 'j':
			cout << "Bitte den Nachnamen der " << (anzahl_personen + 1) << ". Person eingeben: ? ";
			cin >> personen[anzahl_personen].nachname;
			cout << "Bitte den Vornamen der " << (anzahl_personen + 1) << ". Person eingeben: ? ";
			cin >> personen[anzahl_personen].vorname;
			cout << "Bitte das Alter der " << (anzahl_personen + 1) << ". Person eingeben: ? ";
			cin >> personen[anzahl_personen].alter;
			cout << "Bitte das Geschlecht der " << (anzahl_personen + 1) << ". Person eingeben: ? ";
			cin >> personen[anzahl_personen].geschlecht;
			++anzahl_personen;
			break;
		case 'n':
			escape = true;
			break;
		}
		if (escape)
			break;
	} while (true);
	cout << "Ihre Auswahl:\n1 - Weibliche Erwachsene\n2 - Maennliche Erwachsene\n3 - Weibliche Kinder\n4 - Maennliche Kinder\n? ";
	cin >> filter_type;
	filter(personen, anzahl_personen, (FilterTypeEnum)filter_type, count);
	cout << "Summe: " << count << endl;
	system("PAUSE");
	return 0;
}