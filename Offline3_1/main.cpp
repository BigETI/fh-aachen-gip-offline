#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	char c;
	cout << "Bitte geben Sie das Zeichen ein: ? ";
	cin >> c;
	if ((c >= 'a') && (c <= 'z'))
		cout << "Es wurde ein Kleinbuchstabe (a-z) eingegeben.\n";
	else
		cout << "KEIN Kleinbuchstabe (a-z).\n";
	system("PAUSE");
	return 0;
}