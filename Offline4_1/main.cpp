#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	string a;
	cout << "Bitte geben Sie die Zeichenkette ein: ";
	getline(cin, a);
	cout << "Die umgekehrte Zeichenkette lautet: ";

	// Reverse iterator (STL)
	/*for (string::reverse_iterator it = a.rbegin(), end = a.rend(); it != end; ++it)
		cout << (*it);*/

	// Iterate by index
	for (size_t i = a.length(); i != 0ULL; i--)
		cout << a.at(i - 1ULL); // cout << a[i - 1ULL];

	cout << endl;
	system("PAUSE");
	return 0;
}