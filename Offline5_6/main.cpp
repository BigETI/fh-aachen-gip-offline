#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	int ar[] = { 1, 19, 3, 17, 5, 15, 7, 13 };
	const int ar_length = 8;
	int s, f = -1, i;
	
	cout << "Bitte geben Sie die zu suchende ganze Zahl ein: ? ";
	cin >> s;
	for (i = 0; i < ar_length; i++)
	{
		if (ar[i] == s)
		{
			f = i;
			break;
		}
	}
	if (f != -1)
		cout << "Die Zahl " << s << " ist in dem Array an Position " << f << " enthalten.\n";
	else
		cout << "Die Zahl " << s << " ist NICHT in dem Array enthalten.\n";
	system("PAUSE");
	return 0;
}