#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	int arr[5], i, min = 0, max = 0, min_p = 1, max_p = 1;
	//memset(arr, 0, 5 * sizeof(int));
	for (i = 0; i < 5; i++)
		arr[i] = 0;
	for (i = 0; i < 5; i++)
	{
		cout << "Bitte geben Sie die " << (i + 1) << ". Zahl ein: ? ";
		cin >> arr[i];
	}
	min = arr[0];
	max = arr[0];
	for (i = 1; i < 5; i++)
	{
		if (min > arr[i])
		{
			min = arr[i];
			min_p = i + 1;
		}
		if (max < arr[i])
		{
			max = arr[i];
			max_p = i + 1;
		}
	}
	cout << "Die " << min_p << ". Zahl war die kleinste der eingegeben Zahlen und lautet: " << min <<
		"\nDie " << max_p << ". Zahl war der groesste der eingegeben Zahlen und lautet: " << max << endl;
	system("PAUSE");
	return 0;
}