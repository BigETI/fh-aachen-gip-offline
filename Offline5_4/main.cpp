#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	string in;
	cout << "Bitte die Zahl oder das Wort \'ende\' eingeben: ? ";
	cin >> in;
	if (in == string("ende"))
		cout << "Das Programm beendet sich jetzt.\n";
	else
		cout << "Der doppelte Wert betraegt: " << (atoi(in.c_str()) * 2) << endl;
	system("PAUSE");
	return 0;
}