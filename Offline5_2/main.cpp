#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	int arr[6], c = 0, i;
	//memset(arr, 0, 6 * sizeof(int));
	for (i = 0; i < 6; i++)
		arr[i] = 0;
	for (i = 0; i < 9; i++)
	{
		do
		{
			cout << "Bitte geben Sie die " << (i + 1) << "-te Zahl ein: ? ";
			cin >> c;
			if ((c >= 1) && (c <= 6))
				++(arr[c - 1]);
		} while ((c < 1) || (c > 6));
	}
	for (i = 0; i < 6; i++)
		cout << "Die Zahl " << (i + 1) << " wurde " << arr[i] << " mal eingegeben.\n";
	system("PAUSE");
	return 0;
}