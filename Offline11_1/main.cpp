#include <iostream>

using namespace std;

struct w_haeufigkeit {
	char* wort;
	unsigned int haeufigkeit;
};

const unsigned int eingabezeilen_max = 5;
const unsigned int wort_max = 1000;
const unsigned int max_line_length = 80;

void my_memset(void *m, int val, unsigned int size)
{
	for (unsigned int i = 0U; i != size; i++)
		((char *)m)[i] = val;
}

unsigned int my_strlen(const char* const str, const char delim = '\0')
{
	unsigned int ret = 0U;
	for (const char *c = str; (*c != delim) && (*c != '\0'); c++)
		++ret;
	return ret;
}

int my_strcmp(const char* str1, const char* str2)
{
	int ret = 99;
	unsigned int len = my_strlen(str1), i;
	if (len == my_strlen(str2))
	{
		ret = 0;
		for (i = 0U; i != len; i++)
		{
			if (str1[i] != str2[i])
			{
				ret = 99;
				break;
			}
		}
	}
	return ret;
}

char* naechstes_wort(const char* const zeile, unsigned int& pos)
{
	char *ret = nullptr;
	unsigned int c_len, rp;
	if (pos < my_strlen(zeile))
	{
		for (const char *c = &(zeile[pos]); *c; c++)
		{
			++pos;
			if (((*c >= 'a') && (*c <= 'z')) || ((*c >= 'A') && (*c <= 'Z')))
			{
				c_len = my_strlen(c, ' ');
				ret = new char[c_len + 1];
				my_memset(ret, 0, (c_len + 1) * sizeof(char));
				for (rp = 0U; rp != c_len; rp++)
				{
					pos++;
					if (((*c >= 'a') && (*c <= 'z')) || ((*c >= 'A') && (*c <= 'Z')))
						ret[rp] = *c;
					else
						break;
					++c;
				}
				break;
			}
		}
	}
	return ret;
}

void zaehle_wort(char* wort, struct w_haeufigkeit w_haeufigkeiten[], unsigned int& w_count)
{
	for (unsigned int i = 0U; i != w_count; i++)
	{
		if (my_strcmp(w_haeufigkeiten[i].wort, wort) == 0)
		{
			++(w_haeufigkeiten[i].haeufigkeit);
			return;
		}
	}
	w_haeufigkeiten[w_count].wort = wort;
	w_haeufigkeiten[w_count++].haeufigkeit = 1;
}

int main(int argc, char *argv[])
{
	char row[max_line_length];
	struct w_haeufigkeit words[wort_max];
	bool not_found;
	char *word;
	unsigned int pos, w_count = 0U, i;
	my_memset(words, 0, wort_max * sizeof(struct w_haeufigkeit));
	for (i = 0U; i != eingabezeilen_max; i++)
	{
		not_found = true;
		word = nullptr;
		pos = 0U;
		my_memset(row, 0, max_line_length * sizeof(char));
		cout << "Eingabezeile = ? ";
		cin.getline(row, max_line_length);
		while (word = naechstes_wort(row, pos))
		{
			not_found = false;
			zaehle_wort(word, words, w_count);
		}
		if (not_found)
			break;
	}
	for (i = 0U; i != w_count; i++)
		cout << words[i].wort << ": " << words[i].haeufigkeit << endl;
	system("PAUSE");
	return 0;
}