#define AUFGABE_B

#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	int s, a, b;
	cout << "Bitte geben Sie die Gesamtzahl der abgegebenen gueltigen Stimmen ein: ? ";
	cin >> s;
	cout << "Bitte geben Sie die Anzahl Stimmen des ersten Kandidaten ein: ? ";
	cin >> a;
	cout << "Bitte geben Sie die Anzahl Stimmen des zweiten Kandidaten ein: ? ";
	cin >> b;
	cout << "Auf den dritten Kandidaten sind somit " << (s - (a + b)) << " Stimmen entfallen.\n"
#ifdef AUFGABE_B
		"Kandidat 1 erhielt " <<
		((100 * a) / double(s)) <<
		"% der Stimmen.\nKandidat 2 erhielt " <<
		((100 * b) / double(s)) <<
		"% der Stimmen.\nKandidat 3 erhielt " <<
		((100 * (s - (a + b))) / double(s)) <<
		"% der Stimmen.\n"
#endif
		;
	system("PAUSE");
	return 0;
}