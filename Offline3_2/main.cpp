#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	char c;
	cout << "Bitte geben Sie das Zeichen ein: ? ";
	cin >> c;
	switch (((c >= '0') && (c <= '9')) ? 0 : c)
	{
	case 'e': case 'E':
		// Ende
		cout << "Das Programm beendet sich jetzt.\n";
		break;
	case 0:
		// Zahl
		cout << c << " + 5 = " << (int(c - '0') + 5) << endl;
		break;
	default:
		cout << "Weder \'e\' noch noch Ziffer.\n";
	}
	system("PAUSE");
	return 0;
}