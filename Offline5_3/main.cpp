#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	bool arr[6];
	int c = 0, i, r = 0;
	//memset(arr, 0, 6 * sizeof(bool));
	for (i = 0; i < 6; i++)
		arr[i] = false;
	for (i = 0; i < 9; i++)
	{
		do
		{
			cout << "Bitte geben Sie die " << (i + 1) << "-te Zahl ein: ? ";
			cin >> c;
			if ((c >= 1) && (c <= 6))
				arr[c - 1] = true;
		} while ((c < 1) || (c > 6));
	}
	for (i = 0; i < 6; i++)
	{
		if (arr[i])
			++r;
	}
	cout << "In der Eingabe kamen " << r << " unterschiedliche Zahlen vor.\n";
	system("PAUSE");
	return 0;
}