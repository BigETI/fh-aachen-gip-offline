#include <iostream>
#include <string>

using namespace std;

string trimme(string s)
{
	string t, t2, ret;
	int i;
	bool isc = false;

	// Left trim
	for (i = 0; i < s.length(); i++)
	{
		if (isc)
			t += s[i];
		else
		{
			if (isalnum(s[i]))
			{
				isc = true;
				t += s[i];
			}
		}
	}
	isc = false;

	// Right trim
	for (i = t.length() - 1; i >= 0; i--)
	{
		if (isc)
			t2 += t[i];
		else
		{
			if (isalnum(t[i]))
			{
				isc = true;
				t2 += t[i];
			}
			else
				isc = false;
		}
	}
	for (i = t2.length() - 1; i >= 0; i--)
		ret += t2[i];
	return ret;
}

int main(int argc, char *argv[])
{
	string s = "";
	cout << "Bitte geben Sie die Textzeile ein: ? ";
	getline(cin, s);
	cout << "Vorher: " << s << endl;
	cout << "Nachher: " << trimme(s) << endl;
	system("PAUSE");
	return 0;
}