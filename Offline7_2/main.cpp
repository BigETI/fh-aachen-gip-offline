#include <iostream>
#include <string>

using namespace std;

void spalte_ab_erstem(char zeichen, string eingabe, string &erster_teil, string &zweiter_teil)
{
	int i, j;
	erster_teil = eingabe;
	//zweiter_teil.clear();
	zweiter_teil = "";
	for (i = 0; i < eingabe.length(); i++)
	{
		if (eingabe[i] == zeichen)
		{
			//erster_teil.clear();
			erster_teil = "";
			for (j = 0; j < i; j++)
				erster_teil += eingabe[j];
			for (j = i + 1; j < eingabe.length(); j++)
				zweiter_teil += eingabe[j];
			break;
		}
	}
}

int main(int argc, char *argv[])
{
	string t, t1, t2;
	char c;
	cout << "Bitte geben Sie die einzeilige Zeichenkette ein: ? ";
	getline(cin, t);
	cout << "Bitte geben Sie das Zeichen ein: ? ";
	cin >> c;
	spalte_ab_erstem(c, t, t1, t2);
	cout << "Der erste Teil der Zeichenkette lautet: " << t1 <<
		"\nDer zweite Teil der Zeichenkette lautet: " << t2 << endl;
	system("PAUSE");
	return 0;
}