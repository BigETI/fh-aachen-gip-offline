#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	static const int ARR_LEN = 4;
	int count = 0;
	string words[ARR_LEN];
	int i, j, max_len = 0;
	for (i = 0; i < ARR_LEN; i++)
	{
		cout << "Textzeile = ? ";
		getline(cin, words[i]);
		if (words[i].length() <= 0)
			break;
		else
		{
			if (max_len < words[i].length())
				max_len = words[i].length();
			++count;
		}
	}
	for (i = 0; i < count; i++)
	{
		for (j = (max_len - words[i].length()); j > 0; j--)
			cout << '#';
		cout << words[i] << endl;
	}
	system("PAUSE");
}