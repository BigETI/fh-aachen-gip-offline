#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
	double cm;
	cout << "Bitte geben Sie die Seitenlaenge ein (in cm): ? ";
	cin >> cm;
	cout << "Der Umfang des Quadrats betraegt (in cm): " <<
		(4.0 * cm) <<
		"\nDie Flaeche des Quadrats betraegt (in cm*cm): " << (cm * cm) << endl;
	system("PAUSE");
	return 0;
}