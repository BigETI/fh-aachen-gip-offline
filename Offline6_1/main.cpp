#include <iostream>
#include <string>

using namespace std;

string ersetze(string zeile, char zu_ersetzendes_zeichen, string ersatztext)
{
	string ret;
	for (int i = 0; i < zeile.length(); i++)
	{
		if (zeile[i] == zu_ersetzendes_zeichen)
			ret += ersatztext;
		else
			ret += zeile[i];
	}
	return ret;
}

int main(int argc, char *argv[])
{
	string s, r;
	char f;
	cout << "Bitte geben Sie die Textzeile ein: ? ";
	getline(cin, s);
	// bool ist_gerade = ((zahl % 2) == 0);
	cout << "Bitte geben Sie den einzusetzenden Text ein: ? ";
	getline(cin, r);
	cout << "Bitte geben Sie das zu ersetzende Zeichen ein: ? ";
	cin >> f;
	cout << "Ergebnis: " << ersetze(s, f, r) << endl;
	system("PAUSE");
	return 0;
}